from pathlib import Path
import json

ingredients_file = Path.cwd().joinpath("data", "ingredients.json")
ingredients = []

with ingredients_file.open() as readfile:
    ingredients = json.load(readfile)

ingredients = {ingredient["Name"]: {k: v for k, v in ingredient.items() if k != "Name"} for ingredient in ingredients}

def remove_brackets(string):
    return string.replace('(','').replace(')','')

def split_attribute(a_dict, name):
    new_dict = a_dict
    for k, v in a_dict.items():
        sub_dict = v
        boosts = sub_dict[name].split(", ")
        boosts = [boost.rsplit(' ',1) for boost in boosts]
        boosts = {k:remove_brackets(v) for (k,v) in boosts}
        sub_dict[name] = boosts
        new_dict[k] = sub_dict
    return new_dict

power_boost_key = "Meal Power Boosts"
type_boost_key = "Type Boost"
ingredients = split_attribute(ingredients, power_boost_key)
ingredients = split_attribute(ingredients, type_boost_key)

def create_sandwiches(boosts):
    matches = []
    for p, t in boosts:
        matches += create_sandwich(p,t)
    return matches

def create_sandwich(power_boost, type_boost):
    matches = []
    for ingredient, effects in ingredients.items():
        if power_boost in effects[power_boost_key] and type_boost in effects[type_boost_key]:
            matches.append(ingredient)
    return matches

sandwich = create_sandwich("Exp", "Dragon")
print(sandwich)

boosts = [
    # ("Exp", "Fire"),
    ("Exp", "Flying")
]

sandwiches = create_sandwiches(boosts)
print(sandwiches)