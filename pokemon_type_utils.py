import itertools
from enum import Flag, auto
from typing import List, Set, Tuple


class silentset(frozenset):
    def __repr__(self):
        return set(self).__repr__()

    def __str__(self):
        return set(self).__str__()


# class Type:
#     pass

# class Fire(Type):
#     def __init__(self) -> None:
#         super().__init__()

# class Poison(Type):
#     def __init__(self) -> None:
#         super().__init__()

# class Rock(Type):
#     def __init__(self) -> None:
#         super().__init__()

# class Steel(Type):
#     def __init__(self) -> None:
#         super().__init__()

# class Electric(Type):
#     def __init__(self) -> None:
#         super().__init__()
# class Ground(Type):
#     def __init__(self) -> None:
#         super().__init__()
#         self.strengths = {
#             Fire, Electric, Poison, Rock Steel
#         }


class Types(Flag):
    GROUND = auto()
    FIGHT = auto()
    FIRE = auto()
    ICE = auto()
    ROCK = auto()
    ELECTRIC = auto()
    POISON = auto()
    STEEL = auto()
    NORMAL = auto()
    DARK = auto()
    GRASS = auto()
    BUG = auto()
    FLYING = auto()
    DRAGON = auto()
    PSYCHIC = auto()
    GHOST = auto()
    WATER = auto()
    FAIRY = auto()

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name


def smallest_sets(sets):
    min_length = min((len(c) for c in sets if c), default=1)
    return {c for c in sets if len(c) == min_length}


# Key is strong against values
strength_map = {
    Types.GROUND: {Types.FIRE, Types.ELECTRIC, Types.POISON, Types.ROCK, Types.STEEL},
    Types.FIGHT: {Types.NORMAL, Types.ICE, Types.ROCK, Types.DARK, Types.STEEL},
    Types.FIRE: {Types.GRASS, Types.ICE, Types.BUG, Types.STEEL},
    Types.ICE: {Types.GRASS, Types.GROUND, Types.FLYING, Types.DRAGON},
    Types.ROCK: {Types.FIRE, Types.ICE, Types.FLYING, Types.BUG},
    Types.PSYCHIC: {Types.FIGHT, Types.POISON},
    Types.GRASS: {Types.WATER, Types.GROUND, Types.ROCK},
    Types.BUG: {Types.GROUND, Types.PSYCHIC, Types.DARK},
    Types.FLYING: {Types.GRASS, Types.FIGHT, Types.BUG},
    Types.STEEL: {Types.ICE, Types.ROCK, Types.FAIRY},
    Types.FAIRY: {Types.FIGHT, Types.DRAGON, Types.DARK},
    Types.ELECTRIC: {Types.WATER, Types.FLYING},
    Types.GHOST: {Types.PSYCHIC, Types.GHOST},
    Types.DRAGON: {Types.DRAGON},
    Types.DARK: {Types.PSYCHIC, Types.GHOST},
    Types.POISON: {Types.GRASS, Types.FAIRY},
    Types.WATER: {Types.FIRE, Types.GROUND, Types.ROCK},
    Types.NORMAL: set(),
}

# Key is weak against values
weakness_map = {
    Types.GROUND: {Types.WATER, Types.GRASS, Types.ICE},
    Types.FIGHT: {Types.FLYING, Types.PSYCHIC, Types.FAIRY},
    Types.FIRE: {Types.GROUND, Types.ROCK, Types.WATER},
    Types.ICE: {Types.FIGHT, Types.ROCK, Types.STEEL, Types.FIRE},
    Types.ROCK: {Types.FIGHT, Types.GROUND, Types.STEEL, Types.WATER, Types.GRASS},
    Types.PSYCHIC: {Types.BUG, Types.GHOST, Types.DARK},
    Types.GRASS: {Types.FLYING, Types.POISON, Types.BUG, Types.FIRE, Types.ICE},
    Types.BUG: {Types.FLYING, Types.ROCK, Types.FIRE},
    Types.FLYING: {Types.ROCK, Types.ELECTRIC, Types.ICE},
    Types.STEEL: {Types.FIGHT, Types.GROUND, Types.FIRE},
    Types.FAIRY: {Types.POISON, Types.STEEL},
    Types.ELECTRIC: {Types.GROUND},
    Types.GHOST: {Types.GHOST, Types.DARK},
    Types.DRAGON: {Types.ICE, Types.DRAGON, Types.FAIRY},
    Types.DARK: {Types.FIGHT, Types.BUG, Types.FAIRY},
    Types.POISON: {Types.GROUND, Types.PSYCHIC},
    Types.WATER: {Types.GRASS, Types.ELECTRIC},
    Types.NORMAL: set(),
}

# Key resists values
resistance_map = {
    Types.GROUND: {Types.POISON, Types.ROCK},
    Types.FIGHT: {Types.ROCK, Types.BUG, Types.DARK},
    Types.FIRE: {
        Types.BUG,
        Types.STEEL,
        Types.FIRE,
        Types.GRASS,
        Types.ICE,
        Types.FAIRY,
    },
    Types.ICE: {Types.ICE},
    Types.ROCK: {Types.NORMAL, Types.FLYING, Types.POISON, Types.FIRE},
    Types.PSYCHIC: {Types.FIGHT, Types.PSYCHIC},
    Types.GRASS: {Types.GROUND, Types.WATER, Types.GRASS, Types.ELECTRIC},
    Types.BUG: {Types.FIGHT, Types.GROUND, Types.GRASS},
    Types.FLYING: {Types.FIGHT, Types.BUG, Types.GRASS},
    Types.STEEL: {
        Types.NORMAL,
        Types.FLYING,
        Types.ROCK,
        Types.BUG,
        Types.GRASS,
        Types.STEEL,
        Types.PSYCHIC,
        Types.ICE,
        Types.DRAGON,
        Types.FAIRY,
    },
    Types.FAIRY: {Types.FIGHT, Types.BUG, Types.DARK},
    Types.ELECTRIC: {Types.FLYING, Types.ELECTRIC, Types.STEEL},
    Types.GHOST: {Types.POISON, Types.BUG},
    Types.DRAGON: {Types.FIRE, Types.WATER, Types.GRASS, Types.ELECTRIC},
    Types.DARK: {Types.GHOST, Types.DARK},
    Types.POISON: {Types.FIGHT, Types.POISON, Types.BUG, Types.GRASS, Types.FAIRY},
    Types.WATER: {Types.STEEL, Types.FIRE, Types.WATER, Types.ICE},
    Types.NORMAL: set(),
}

# Key is immune to value
immune_map = {
    Types.GROUND: {Types.ELECTRIC},
    Types.FIGHT: set(),
    Types.FIRE: set(),
    Types.ICE: set(),
    Types.ROCK: set(),
    Types.PSYCHIC: set(),
    Types.GRASS: set(),
    Types.BUG: set(),
    Types.FLYING: {Types.GROUND},
    Types.STEEL: {Types.POISON},
    Types.FAIRY: {Types.DRAGON},
    Types.ELECTRIC: set(),
    Types.GHOST: {Types.NORMAL, Types.FIGHT},
    Types.DRAGON: set(),
    Types.DARK: {Types.PSYCHIC},
    Types.POISON: set(),
    Types.WATER: set(),
    Types.NORMAL: {Types.GHOST},
}


def filter_types(types: Set[Types], movesets: Set[Set[Types]]):
    new_movesets = set()
    for moveset in movesets:
        test_set = set(moveset)
        if test_set.intersection(types) == types:
            new_movesets.add(moveset)
    return new_movesets


def get_excludes(excludes: Set[Types], movesets: List[Set[Types]]):
    new_movesets = set()
    for moveset in movesets:
        test_set = set(moveset)
        if len(test_set - excludes) == len(test_set):
            new_movesets.add(moveset)
    return new_movesets


def get_movesets(
    types: Tuple[Types], includes: Set[Types] = set(), excludes: Set[Types] = set()
) -> Set[Set[Types]]:
    movesets = list(itertools.combinations(strength_map.keys(), 4))
    movesets = filter_types(set(types), movesets)
    if includes:
        movesets = filter_types(includes, movesets)
    if excludes:
        movesets = get_excludes(excludes, movesets)
    return movesets


def get_strengths(moveset: Set[Types]) -> set:
    result = set()
    if moveset:
        for move in moveset:
            result = result.union(strength_map[move])
    return result


def get_best_coverage(
    types: Tuple[Types], includes: Set[Types] = set(), excludes: Set[Types] = set()
) -> Set[Types]:
    movesets = get_movesets(types, includes, excludes)
    results = {moveset: get_strengths(moveset) for moveset in movesets}
    coverage_size = max(len(strengths) for strengths in results.values())
    results = {
        moveset: strengths
        for moveset, strengths in results.items()
        if len(strengths) == coverage_size
    }
    for moveset, strengths in results.items():
        print(f"{moveset} is strong against {len(strengths)} types")
    return set(results.keys())


def check_coverage(name, moveset: Set[Types], includes, weaknesses):
    strengths = get_strengths(moveset | includes)
    coverage = weaknesses - strengths
    if coverage:
        print(f"{name} has no cover for {coverage}")
        print(f"Try any of {get_type_weaknesses(coverage)}")


def get_type_weaknesses(types: Set[Types]):
    result = set()
    for t in types:
        result = result.union(weakness_map[t])
    return result


def get_pokemon_weaknesses(types: Tuple[Types]):
    weaknesses = set()
    if len(types) == 2:
        type1, type2 = types
        weaknesses.update(
            [
                t
                for t in weakness_map[type1]
                if t
                not in strength_map[type2] | resistance_map[type2] | immune_map[type2]
            ]
        )
        weaknesses.update(
            [
                t
                for t in weakness_map[type2]
                if t
                not in strength_map[type1] | resistance_map[type1] | immune_map[type1]
            ]
        )
    else:
        weaknesses.update([t for t in weakness_map[types]])
    return weaknesses


def get_cover_strength(cover: Set[Types]) -> int:
    return len(get_strengths(cover))


def filter_covers(covers: Set[Set[Types]]) -> Set[Set[Types]]:
    strongest = max(get_cover_strength(c) for c in covers)
    return silentset(c for c in covers if get_cover_strength(c) == strongest)


def cover_weaknesses(types: Tuple[Types], excludes: Set[Types] = set()) -> Set[Types]:
    weaknesses = get_pokemon_weaknesses(types)
    cover = set()
    for weakness in weaknesses:
        cover.update(weakness_map[weakness])
    cover -= excludes
    if len(cover) < (4 - len(types)):
        print(
            "Failed to find a valid moveset, due to too few weaknesses, or too many excludes"
        )
        return set()
    movesets = set(itertools.combinations(cover, 4 - len(types)))
    movesets = set(tuple(types) + m for m in movesets)
    print(f"Looking for cover against {weaknesses} using any of {cover}")
    if not movesets:
        print("Failed to find a valid moveset")
        return set()
    return filter_covers(movesets)


team = {
    "Infernape": {"types": (Types.FIRE, Types.FIGHT)},
    "Electivire": {"types": (Types.ELECTRIC, Types.FIGHT)},
    "Riolu": {"types": (Types.FIGHT)},
    "Gardevoir": {"types": (Types.PSYCHIC, Types.FAIRY)},
    "Swampert": {"types": (Types.WATER, Types.GROUND)},
    "Umbreon": {"types": (Types.DARK)},
}

for pokemon, type_info in team.items():
    print("=" * 100)
    print(pokemon)
    types = type_info["types"]
    includes = type_info["includes"] if "includes" in type_info else set()
    excludes = type_info["excludes"] if "excludes" in type_info else set()
    wide_coverage = get_best_coverage(types, includes, excludes)
    weakness_coverage = cover_weaknesses(types, excludes)
    print(f"Widest coverage movesets: {wide_coverage}")
    if weakness_coverage and next(iter(weakness_coverage)):
        print(f"Weakness coverage movesets:")
        for moveset in weakness_coverage:
            print(f"{moveset} is strong against {len(get_strengths(moveset))} types")
    print()
