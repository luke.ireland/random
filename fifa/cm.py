import json
from pathlib import Path

team = dict()
team_data_path = Path("team.json")
if team_data_path.exists():
    with team_data_path.open() as readfile:
        team = json.load(readfile)

transfers = dict()
transfers_data_path = Path("transfers.json")
if transfers_data_path.exists():
    with transfers_data_path.open() as readfile:
        transfers = json.load(readfile)


def show_main_menu():
    print()
    print("Main Menu")
    print("(0): Exit")
    print("(1): Team Menu")
    print("(2): Transfer Menu")


def show_team_menu():
    print()
    print("Team Menu")
    print("(0): Return to Main Menu")
    print("(1): Enter new player info")
    print("(2): Update player info")
    print("(3): Delete player info")
    print("(4): Display information about a player")
    print("(5): Display information about a position")
    print("(6): Display information about all players")
    print("(7): Display score (OVR-AGE) for a player")
    print("(8): Display score (OVR-AGE) for a position")
    print("(9): Display score (OVR-AGE) for all players")
    print()


def show_transfer_menu():
    print()
    print("Transfer Menu")
    print("(0): Return to Main Menu")
    print("(1): Enter new player info")
    print("(2): Update player info")
    print("(3): Delete player info")
    print("(4): Display information about a player")
    print("(5): Display information about a position")
    print("(6): Display information about all players")
    print("(7): Display score (OVR-AGE) for a player")
    print("(8): Display score (OVR-AGE) for a position")
    print("(9): Display score (OVR-AGE) for all players")
    print("(10): Show possible replacements")
    print("(11): Buy Players")
    print()


def update_file(data, path):
    with path.open("w") as writefile:
        json.dump(data, writefile)


def add_player(data, path):
    first_name = input("Enter player's First Name: ")
    last_name = input("Enter player's Last Name: ")
    age = int(input("Enter player's age: "))
    ovr = int(input("Enter player's OVR: "))
    position = input("Enter player's position: ")
    name = "{} {}".format(first_name, last_name)
    if (first_name or last_name) and age and ovr and position:
        data[name] = {"firstname": first_name, "lastname": last_name, "age": age, "ovr": ovr, "position": position}
        update_file(data, path)


def update_player(data, path):
    name = input("Enter player's full name: ")
    age = input("Enter new age (Leave blank for no change): ")
    ovr = input("Enter new OVR (Leave blank for no change): ")
    pos = input("Enter new position (Leave blank for no change): ")
    if name in data:
        if age:
            data[name]["age"] = int(age)
        if ovr:
            data[name]["ovr"] = int(ovr)
        if pos:
            data[name]["position"] = pos
        update_file(data, path)
    else:
        print("Player not found")


def delete_player(data, path):
    name = input("Enter player's full name: ")
    if name in data:
        del data[name]
        update_file(data, path)
    else:
        print("Player not found")


def print_player(player):
    print(
        "{} {}: {}/{}/{} - {}".format(
            player["firstname"],
            player["lastname"],
            player["age"],
            player["ovr"],
            player["position"],
            player["score"] if "score" in player else "",
        )
    )


def display_player(data):
    name = input("Enter player's full name: ")
    if name in data:
        player = data[name]
        print_player(player)


def display_position(data):
    pos = input("Enter desired position: ")
    pos_players = [player for player in data.values() if player["position"] == pos]
    for player in sort_players(pos_players):
        print_player(player)


def view_all(data):
    for player in sort_players(data.values()):
        print_player(player)

def sort_players(players: list) -> list:
    return sorted(players, key=lambda player: player["score"], reverse=True)

def score(data):
    players_score = dict()
    for name, player in data.items():
        ovr = player["ovr"]
        age = player["age"]
        score = int((ovr - age) * (ovr / age))
        players_score[name] = score
    for player in players_score:
        data[player]["score"] = players_score[player]
    data = {k: v for k, v in sorted(data.items(), key=lambda item: item[1]["score"])}


def get_replacements():
    replacements = []
    score(team)
    score(transfers)
    for player_name, player_stats in team.items():
        for replacement_name, replacement_stats in transfers.items():
            if player_stats["position"] == replacement_stats["position"]:
                if replacement_stats["score"] > player_stats["score"]:
                    player_score = player_stats["score"]
                    player_age = player_stats["age"]
                    player_ovr = player_stats["ovr"]
                    replacement_score = replacement_stats["score"]
                    replacement_age = replacement_stats["age"]
                    replacement_ovr = replacement_stats["ovr"]
                    replacements.append(
                        (
                            player_name,
                            player_age,
                            player_ovr,
                            replacement_name,
                            replacement_age,
                            replacement_ovr,
                            replacement_score - player_score,
                        )
                    )
    #             player_ovr = player_stats["ovr"]
    #             replacement_ovr = replacement_stats["ovr"]
    #             if replacement_ovr > player_ovr:
    #                 ovr_diff = replacement_ovr - player_ovr
    #                 player_age = player_stats["age"]
    #                 replacement_age = replacement_stats["age"]
    #                 if (replacement_age - ovr_diff) <= player_age:
    #                     replacements.append(
    #                         (
    #                             player_name, replacement_name,
    #                             ovr_diff,
    #                             player_age - replacement_age
    #                         )
    #                     )
    return replacements


def show_replacements():
    replacements = get_replacements()
    replacements = [replacement for replacement in replacements if replacement[6] > 50]
    for replacement in replacements:
        print(
            "Replace {} ({}/{}) with {} ({}/{}) (Score Diff: {})".format(
                replacement[0],
                replacement[1],
                replacement[2],
                replacement[3],
                replacement[4],
                replacement[5],
                replacement[6],
            )
        )


def get_player(data):
    name = input("Enter player's full name: ")
    if name in data:
        return (name, data[name])
    return None


def buy_players():
    name, stats = get_player(transfers)
    if name:
        del transfers[name]
        update_file(transfers, path)
        team[name] = stats
    else:
        print("Invalid player/Player Not Found")


def get_choice():
    try:
        choice = int(input())
        return choice
    except ValueError:
        pass


def use_choice(choice, data, path):
    if choice == 1:
        add_player(data, path)
    elif choice == 2:
        update_player(data, path)
    elif choice == 3:
        delete_player(data, path)
    elif choice == 4:
        display_player(data)
    elif choice == 5:
        display_position(data)
    elif choice == 6:
        data = {k: v for k, v in sorted(data.items(), key=lambda item: item[1]["ovr"])}
        view_all(data)
    elif choice == 7:
        score(data)
        display_player(data)
    elif choice == 8:
        score(data)
        display_position(data)
    elif choice == 9:
        score(data)
        view_all(data)
    else:
        print("Please enter a valid option")


while True:
    show_main_menu()
    choice = -1
    choice = get_choice()
    if choice == 0:
        break
    elif choice == 1:
        data = team
        path = team_data_path
        while True:
            show_team_menu()
            choice = get_choice()
            if choice == 0:
                break
            else:
                use_choice(choice, data, path)
    elif choice == 2:
        data = transfers
        path = transfers_data_path
        while True:
            show_transfer_menu()
            choice = get_choice()
            if choice == 0:
                break
            elif choice == 10:
                show_replacements()
            elif choice == 11:
                buy_players()
            else:
                use_choice(choice, data, path)
    else:
        print("Please enter a valid option")
