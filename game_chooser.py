from numpy.random import choice

played_weight = 0.1
dlc_weight = 0.3
new_weight = 0.7
multi_weight = 0.4

include_played = False
include_dlc = True
include_new = True
include_multi = True

played_games = [
    "Satisfactory",
    "Transport Fever 2",
    "The Sims 4",
]

played_with_dlc = [
    "Stellaris Federations",
    "Phoenix Point",
    "Sid Meier's Civilization VI",
    "Age of Wonders - Planetfall"
]

new_games = [
    "MechWarrior 5 - Mercenaries",
    "Sekiro - Shadows Die Twice",
    "XCOM - Chimera Squad",
    "Pax Nova",
    "Yes, Your Grace",
    "Gears Tactics"
]

multi_games = [
    "World War Z",
    "Modern Warfare",
    "FIFA"
]

games = []
if include_played:
    games += played_games
if include_dlc:
    games += played_with_dlc
if include_new:
    games += new_games
if include_multi:
    games += multi_games

probability_distribution = []
for game in games:
    if game in played_games:
        probability_distribution.append(played_weight)
    elif game in played_with_dlc:
        probability_distribution.append(dlc_weight)
    elif game in new_games:
        probability_distribution.append(new_weight)
    elif game in multi_games:
        probability_distribution.append(multi_weight)
    else:
        print("Game {} not found".format(game))

probability_distribution = [
    prob/sum(probability_distribution) for prob in probability_distribution]

game = choice(games, 1, p=probability_distribution)[0]

print(game)
