from PIL import Image
from pathlib import Path

discord_limit_bytes = 8000000

screenshot_path = Path("/mnt/c/Users/Luke/Pictures/Screenshots")
images = [image for image in screenshot_path.iterdir() if image.suffix == ".png" and image.stat().st_size > discord_limit_bytes]
images = {image:Image.open(image) for image in images}

for name, image in images.items():
    print("Shrinking {}".format(name))
    (width, height) = (image.width // 2, image.height // 2)
    image = image.resize((width, height))
    image.save(name)