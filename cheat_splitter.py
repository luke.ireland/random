from os import unlink
from pathlib import Path

main_cheats_file = Path(r"C:\Users\Luke\Downloads\\21-11-21 Main codes\\atmosphere\contents\\0100000011D90000\cheats\D9E96FB92878E345.txt")
game_mod_folder = Path.home().joinpath("AppData/Roaming/Ryujinx/mods/contents/0100000011d90000")

cheats = []
with main_cheats_file.open() as read_file:
    cheats = read_file.read().split("\n\n")

dirs = [p for p in game_mod_folder.iterdir() if p.is_dir()]
for dir in dirs:
    dir.unlink()

for cheat in cheats:
    cheat_name = cheat.splitlines()[0]
    cheat_name = cheat_name.replace("/", "-")
    cheat_dir = game_mod_folder.joinpath(cheat_name, "cheats")
    cheat_dir.mkdir(parents=True)
    cheat_file = cheat_dir.joinpath("D9E96FB92878E345.txt")
    with cheat_file.open('w') as write_file:
        write_file.write(cheat)
