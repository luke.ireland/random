import time
traders = {
    "Meetup Spot": [],
    "East Lake Axewell": [],
    "Dappled Grove": [],
    "Giant's Seat": [],
    "Bridge Field": [],
    "Hammerlocke Hills": [],
    "Giant's Cap": []
}

pokeballs = [
    "Net Ball", "Dive Ball", "Nest Ball", "Repeat Ball", "Timer Ball",
    "Luxury Ball", "Dusk Ball", "Heal Ball", "Quick Ball"
]


def get_start():
    day = time.localtime().tm_yday
    offset = 3 # Changes when I use the time glitch
    return (day % 49)-offset


count = 0
for trader in traders.keys():
    first = get_start()+(count*7)
    second = first+24
    third = first+42
    fourth = (first+67) % 99
    fifth = (first+96) % 99
    pokeball = ""
    if first == 47:
        pokeball = "Repeat Ball"
    elif first == 48:
        pokeball = "Quick Ball"
    elif first == 49:
        pokeball = "Heal Ball"
    else:
        pokeball = pokeballs[first % 9]
    traders[trader] = [first, second, third, fourth, fifth, pokeball]
    count += 1

trs = input("Enter your desired TRs:\n").split()
trs = [int(tr) for tr in trs]
for tr in trs:
    closest = 500
    for trader, items in traders.items():
        if tr in items:
            print("Visit Trader at {} for TR{}".format(trader, tr))
            break
        for item in items[:-1]:
            diff = tr-item
            if diff > 0 and diff < closest:
                closest = diff
    if closest != 500:
        print("Try Trader at {} in {} days for TR{}".format(
            trader, closest, tr))
