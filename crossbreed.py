"""

lower = red
UPPER = GREEN

GGGGYY - Aim (4 G, 2 Y)

"""
plants = [
    "GGHYGx", "GYHxHH", "GGHHHx", "HGHwGY", "HYHwGH", "HGYxYG", "HYYHHw", "HYGHGx",
    "wYHHGH", "wGGHHH", "xGGGHH", "YGGwHH", "YGHwGG", "YYYGYG"
]

solutios = ["GxYYGG"]


def predict(chosen):
    prediction = {i:"?" for i in range(6)}
    slot_dict = check_coverage(chosen)
    for slot, plants in slot_dict.items():
        if len(plants) > 0:
            genes = [plant[slot] for plant in plants]
            gene_freq = {gene: genes.count(gene) for gene in genes}
            best_genes = get_compat(gene_freq)
            best_gene = get_fittest(best_genes)
            prediction[slot] = best_gene
        else:
            print("No valid plant for slot {}".format(slot))
    print("".join(prediction.values()))

def check_coverage(plants):
    used_slots = get_slots(plants)
    slot_dict = {slot: [] for slot in used_slots}
    for plant in plants:
        for slot in used_slots:
            if plant[slot] == plant[slot].upper():
                slot_dict[slot].append(plant)
    return slot_dict

def get_compat(plants):
    return [keys for keys, values in plants.items() if values == max(plants.values())]

def get_fittest(plants):
    fitness_dict = dict()
    for plant in plants:
        fitness = 0
        for gene in plant:
            if gene == "Y" or gene == "G":
                fitness += 1
        fitness_dict[plant] = fitness
    return max(fitness_dict, key=fitness_dict.get)


def get_bad_pos(plant):
    bad_gene_pos = -1
    for gene in plant:
        if gene == gene.lower():
            bad_gene_pos = plant.index(gene)
    return bad_gene_pos


def get_slots(plants):
    return [get_bad_pos(plant) for plant in plants]


compat_dict = {plant: 0 for plant in plants}
plant_dict = dict()
for plant1 in plants:
    matches = []
    bad_gene_pos = get_bad_pos(plant1)
    for plant2 in plants:
        if plant2[bad_gene_pos] == plant2[bad_gene_pos].upper():
            match_bad_gene_pos = get_bad_pos(plant2)
            if plant1[match_bad_gene_pos] == plant1[match_bad_gene_pos].upper():
                matches.append(plant2)
    plant_dict[plant1] = matches

for matches in plant_dict.values():
    for match in matches:
        compat_dict[match] += 1

compat_dict = {k: v for k, v in sorted(
    compat_dict.items(), key=lambda item: item[1], reverse=True)}

best_compat = get_compat(compat_dict)

winner = get_fittest(best_compat)

chosen = [winner, get_fittest(plant_dict[winner])]

while (len(chosen) < 4):
    used_slots = get_slots(chosen)
    last = chosen[-1]
    matches = [match for match in plant_dict[last] if match not in chosen]
    compatible = []
    compat = 0
    for match in matches:
        pos = get_bad_pos(match)
        for plant in chosen:
            if plant[pos] == plant[pos].upper():
                compat += 1
        if compat >= 2 and get_bad_pos(match) not in used_slots:
            compatible.append(match)
    if compatible:
        chosen.append(get_fittest(compatible))
    else:
        print("No valid combinations")
        predict(chosen)
        break

print(chosen)